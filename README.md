
**What?** Trivial project to demonstrate that `qmake` is unable to correctly generate C++ dependencies, which is leading to broken incremental builds.

**Why?** This is happening because dependencies merged into the generated `Makefile`, making it unable to reevaluate them without restarting whole configuration.

**When?** This issue is not new, it was like that since the day one of `qmake` birth.

**Proofs!** Follow the steps below.

1. Build base commit

        git checkout step1
        qmake && make && ./run

    Output: `value = 123`
    OK

2. Move `value` from `foo.h` to `bar.h`

        git checkout step2
        make && ./run

    Output: `value = 123`
    OK

3. Change `value = 123` to `value = 456`

        git checkout step3
        make && ./run

    Output: `value = 123`
    FAIL!

    Expected: `value = 456`
